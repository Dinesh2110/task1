export class User {
  constructor(
    public name: string,
    public mailid: string,
    public phone: number,
    public topic: string,
    public timepreference: string,
    public subscribe: boolean
  ) {}
}
