import { Component } from '@angular/core';
import { User } from '../user';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css'],
})
export class FormsComponent {
  topics = ['Good', 'Average', 'Bad'];
  constructor(
    private toastr: ToastrService,
    private route: Router,
    private router: ActivatedRoute
  ) {}
  usermodel = new User(
    'dinesh',
    'dinesh@123.com',
    9876568789,
    'angular',
    'morning',
    true
  );
  onSubmit(myForm: NgForm) {
    console.log(myForm);
  }
  showToast() {
    this.toastr.success('DONE', 'SUBSCRIBE SUCCESSFULLY');

    setInterval(() => {
      this.route.navigate(['/run'], { relativeTo: this.router });
    }, 3000);
  }
}
