import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.css'],
})
export class RunComponent implements OnInit {
  ngOnInit(): void {
    setInterval(() => this.runIncrement(this.indrun), 3000);
    setInterval(() => this.wiRunIncrement(this.wirun), 6000);
  }
  runIncrement(numReceived: number) {
    this.indrun += Math.ceil(Math.random());
  }
  wiRunIncrement(Received: number) {
    this.wirun += Math.ceil(Math.random());
  }
  constructor(private route: Router, private router: ActivatedRoute) {}

  indrun = 0;
  wirun = 0;
  wiwicket = 2;

  goTo() {
    this.route.navigate(['/forms'], { relativeTo: this.router });
  }
}
